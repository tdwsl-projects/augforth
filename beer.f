\ 99 bottles of beer on the wall

: .bottle
  dup if dup . else ." no " then ." bottle"
  dup 1- if [char] s emit then space ;

: .bottles ( n -- n-1 )
  .bottle ." of beer on the wall," cr
  .bottle ." of beer~" cr
  ." take one down," cr ." pass it around," cr
  1- .bottle ." of beer on the wall." cr
  cr ;

: 99bottles 99 begin .bottles ?dup 0= until ;

99bottles bye

