; forth interpreter for x86 linux

    section .text
    global _start

sys_exit: equ 1
sys_write: equ 4
sys_read: equ 3
sys_write: equ 4
sys_open: equ 5
sys_close: equ 6
sys_ftruncate: equ 93

file_writeflags: equ 0102o
file_readflags: equ 0000o
file_mode: equ 0600o

_start:
    mov edx,stack_end
    mov dword [last],lastinit
    mov dword [here],dict
    mov dword [status],0
    mov dword [base],10
    mov dword [nextfun],getnextc
    mov dword [msp],mstack_end

    call interpreter

    mov eax,sys_exit
    int 0x80

include:
    push edx

    call nullterm
    mov eax,sys_open
    mov ecx,file_readflags
    mov edx,file_mode
    int 0x80
    cmp eax,-2
    jz failopen

    pop edx

    mov ecx,[msp]
    mov ebx,[nextfun]
    sub ecx,4
    mov [ecx],ebx
    mov ebx,[fd]
    sub ecx,4
    mov [ecx],ebx
    mov [fd],eax
    mov dword [nextfun],fgetnextc
    mov [msp],ecx
    ret

fgetnextc:
    mov eax,sys_read
    mov ebx,[fd]
    mov ecx,esp
    dec ecx
    mov edx,1
    int 0x80
    xor eax,eax
    mov al,[ecx]

    ;push edi
    ;push eax
    ;call puth
    ;mov al,32
    ;call putc
    ;pop eax
    ;pop edi
    push edi ; < ~~~  this fixes it (but why?)
    pop edi  ; <

    cmp al,8
    jz fgetnextc_0

    ret

fgetnextc_0:
    mov ebx,[fd]
    mov eax,sys_close
    int 0x80

    mov ecx,[msp]
    mov eax,[ecx]
    add ecx,4
    mov [fd],eax
    mov eax,[ecx]
    add ecx,4
    mov [nextfun],eax
    mov [msp],ecx
    xor al,al
    ret

restore:
    push edx

    call nullterm
    mov eax,sys_open
    mov ecx,file_readflags
    mov edx,file_mode
    int 0x80
    cmp eax,-2
    jz failopen

    mov ebx,eax

    mov eax,sys_read
    mov ecx,esp
    sub ecx,4
    mov edx,4
    int 0x80
    mov eax,[ecx]
    add eax,dict
    mov [here],eax

    mov eax,sys_read
    mov edx,4
    int 0x80
    mov eax,[ecx]
    add eax,dict
    mov [last],eax

    mov eax,sys_read
    mov ecx,dict
    mov edx,0xffffffff
    int 0x80

    mov eax,sys_close
    int 0x80

    pop edx
    ret

save:
    push edx

    call nullterm
    mov eax,sys_open
    mov ecx,file_writeflags
    mov edx,file_mode
    int 0x80
    cmp eax,-2
    jz failopen

    mov ebx,eax

    mov eax,sys_ftruncate
    xor ecx,ecx
    int 0x80

    mov ecx,esp
    sub ecx,4
    mov eax,[here]
    sub eax,dict
    mov [ecx],eax
    mov eax,sys_write
    mov edx,4
    int 0x80

    mov eax,[last]
    sub eax,dict
    mov [ecx],eax
    mov eax,sys_write
    mov edx,4
    int 0x80

    mov eax,sys_write
    mov ecx,dict
    mov edx,[here]
    sub edx,ecx
    int 0x80

    mov eax,sys_close
    int 0x80

    pop edx
    ret

failopen:
    mov eax,failopen_msg
    mov ebx,failopen_msg_end-failopen_msg
    call putsn
    mov eax,nextbuf
    call count
    call putsn
    mov al,10
    call putc

    mov eax,sys_exit
    int 0x80

failopen_msg:
    db "failed to open "
failopen_msg_end:

nullterm:
    mov edi,nextbuf
    mov esi,eax
    mov ecx,ebx
    mov ebx,edi
    rep movsb
    xor al,al
    stosb
    ret

interpreter:
    push edx
    call getnext
    call findword
    pop edx
    jz interpreter_n

    mov eax,[status]
    shl eax,2
    add eax,ecx
    add eax,4
    mov eax,[eax]

    call eax

    jmp interpreter

interpreter_n:
    call number
    jz notfound

    mov eax,[status]
    or eax,eax
    jz interpreter_n0

    mov eax,ecx
    push dword interpreter
    jmp literal

interpreter_n0:
    sub edx,4
    mov [edx],ecx
    jmp interpreter

notfound:
    call putsn
    mov eax,notfound_msg
    mov ebx,notfound_msg_end-notfound_msg
    call putsn

cancelcol:
    mov eax,[status]
    or eax,eax
    jz cancelcol_0

    mov eax,[newlast]
    mov [here],eax

cancelcol_0:
    ret

notfound_msg:
    db " ?",10,0
notfound_msg_end:

literal:
    mov edi,[here]

    ; sub edx,4 ; mov dword [edx],#
    mov dword [edi],0xc704ea83
    add edi,4
    mov byte [edi],0x02
    inc edi

    stosd
    mov [here],edi
    ret

number:
    call storeabd

    or ebx,ebx
    jz number_n

    mov esi,eax
    mov edi,ebx
    xor bl,bl
    xor ecx,ecx
    xor eax,eax

    mov al,[esi]
    cmp al,'-'
    jnz number_nn

number_neg:
    inc esi
    dec edi
    jz number_n

    inc bl
    jmp number_0

number_nn:
    cmp al,'#'
    jnz number_nd

    inc esi
    dec edi
    jz number_n
    mov eax,[base]
    push eax
    mov dword [base],10
    call number_nag
    pop esi
    mov [base],esi
    ret

number_nd:
    cmp al,'$'
    jnz number_nh

    inc esi
    dec edi
    jz number_n
    mov eax,[base]
    push eax
    mov dword [base],16
    call number_nag
    pop esi
    mov [base],esi
    ret

number_nh:
    cmp al,'%'
    jnz number_0

    inc esi
    dec edi
    jz number_n
    mov eax,[base]
    push eax
    mov dword [base],2
    call number_nag
    pop esi
    mov [base],esi
    ret

number_nag:
    mov al,[esi]
    cmp al,'-'
    jz number_neg

number_0:
    mov dl,[base]
    cmp dl,11
    jb number_b

    mov bh,[base]
    add bh,'a'

number_a0:
    lodsb
    cmp al,'0'
    jb number_a2
    cmp al,'9'
    ja number_a2

    sub al,'0'

number_a1:
    xchg ecx,eax
    mov edx,[base]
    mul edx
    xchg ecx,eax
    add ecx,eax
    dec edi
    jnz number_a0

    jmp number_t

number_a2:
    cmp al,'a'
    jb number_n
    cmp al,bh
    ja number_n

    sub al,'a'-10
    jmp number_a1

number_b:
    mov bh,[base]
    add bh,'0'

number_b0:
    lodsb
    cmp al,'0'
    jb number_n
    cmp al,bh
    ja number_n

    sub al,'0'
    xchg ecx,eax
    mov edx,[base]
    mul edx
    xchg ecx,eax
    add ecx,eax
    dec edi
    jnz number_b0

number_t:
    or bl,bl
    jz number_t0

    neg ecx

number_t0:
    call loadabd
    or esi,esi
    ret

number_n:
    call loadabd
    xor ecx,ecx
    ret

storeabd:
    mov [temp1],eax
    mov [temp2],ebx
    mov [temp3],edx
    ret

loadabd:
    mov eax,[temp1]
    mov ebx,[temp2]
    mov edx,[temp3]
    ret

getnext:
    push edx

getnext_0:
    mov edi,nextbuf
    mov esi,0

getnext_1:
    call [nextfun]
    cmp al,33
    jb getnext_2

    stosb
    inc esi
    jmp getnext_1

getnext_2:
    or esi,esi
    jnz getnext_3

    or al,al
    jnz getnext_0

getnext_3:
    pop edx

    mov ebx,esi
    mov eax,nextbuf
    or ebx,ebx
    jz getnext
    ret

getnextc:
    mov ecx,esp
    ;sub ecx,4
    dec ecx
    mov edx,1
    mov eax,sys_read
    mov ebx,2
    int 0x80
    mov al,[ecx]
    ret

findword:
    push eax
    mov ecx,[last]
    xor edx,edx

findword_0:
    mov dl,[ecx]
    inc ecx
    call streq
    jz findword_1

    add ecx,edx
    mov ecx,[ecx]
    or ecx,ecx
    jnz findword_0

    pop eax
    ret ; fail

findword_1: ; found
    add ecx,edx
    or ecx,ecx
    pop eax
    ret

streq:
    pushad
    mov esi,eax
    cmp ebx,edx
    jnz streq_n

streq_0:
    lodsb
    mov ah,[ecx]
    cmp al,ah
    jnz streq_n

    inc ecx
    dec edx
    jnz streq_0

streq_t:
    xor edx,edx
    popad
    ret

streq_n:
    popad
    or eax,eax
    ret

count:
    mov esi,eax
    mov ebx,eax

count_0:
    lodsb
    or al,al
    jnz count_0

    sub esi,ebx
    mov eax,ebx
    mov ebx,esi
    ret

puth:
    push eax
    mov ebx,eax
    mov edi,buf1+8
    std

puth_0:
    mov eax,ebx
    shr ebx,4
    and al,0xf
    cmp al,10
    jb puth_1

    add al,'a'-10
    jmp puth_2

puth_1:
    add al,'0'
puth_2:
    stosb
    or ebx,ebx
    jnz puth_0

    cld
    inc edi
    mov eax,edi
    neg edi
    inc edi
    add edi,buf1+8
    mov ebx,edi
    call putsn

    pop eax
    ret

putc:
    pushad
    mov ecx,esp
    dec ecx
    mov [ecx],al
    mov edx,1
    mov eax,sys_write
    mov ebx,1
    int 0x80
    popad
    ret

putsn:
    pushad
    mov ecx,eax
    mov edx,ebx
    mov eax,sys_write
    mov ebx,1
    int 0x80
    popad
    ret

compile:
    mov edi,[here]
    mov byte [edi],0xe8 ; call rel
    inc edi
    add ecx,4
    mov eax,[ecx]
    sub eax,edi
    sub eax,4
    stosd
    mov [here],edi
    ret

; dictionary

wemit:
    db 4,"emit"
    dd 0
    dd wemit_0
    dd compile
wemit_0:
    mov ecx,esp
    sub ecx,4
    mov eax,[edx]
    add edx,4
    mov [ecx],eax
    mov eax,sys_write
    mov ebx,1
    mov edi,edx
    mov edx,4
    int 0x80
    mov edx,edi
    ret

wadd:
    db 1,"+"
    dd wemit
    dd wadd_0
    dd compile
wadd_0:
    mov eax,[edx]
    add edx,4
    add [edx],eax
    ret

wcol:
    db 1,":"
    dd wadd
    dd wcol_0
    dd compile
wcol_0:
    push edx
    call getnext
    pop edx
    mov edi,[here]
    mov [newlast],edi
    mov ecx,ebx
    mov esi,eax
    mov al,cl
    stosb
    rep movsb

    mov eax,[last]
    stosd
    mov eax,edi
    add eax,8
    stosd
    mov eax,compile
    stosd

    mov [here],edi
    mov dword [status],1
    ret

wsemi:
    db 1,";"
    dd wcol
    dd wsemi_0
    dd wsemi_0
wsemi_0:
    mov eax,[newlast]
    mov [last],eax
    mov dword [status],0
    mov edi,[here]
    mov al,0xc3
    stosb
    mov [here],edi
    ret

wswap:
    db 4,"swap"
    dd wsemi
    dd wswap_0
    dd compile
wswap_0:
    mov ecx,edx
    add ecx,4
    mov eax,[edx]
    mov ebx,[ecx]
    mov [ecx],eax
    mov [edx],ebx
    ret

wdup:
    db 3,"dup"
    dd wswap
    dd wdup_0
    dd compile
wdup_0:
    mov eax,[edx]
    sub edx,4
    mov [edx],eax
    ret

wover:
    db 4,"over"
    dd wdup
    dd wover_0
    dd compile
wover_0:
    add edx,4
    mov eax,[edx]
    sub edx,8
    mov [edx],eax
    ret

wdrop:
    db 4,"drop"
    dd wover
    dd wdrop_0
    dd compile
wdrop_0:
    add edx,4
    ret

wrph:
    db 2,">r"
    dd wdrop
    dd wrph_0
    dd compile
wrph_0:
    mov eax,[edx]
    add edx,4
    pop ebx
    push eax
    jmp ebx

wrpl:
    db 2,"r>"
    dd wrph
    dd wrpl_0
    dd compile
wrpl_0:
    pop ebx
    pop eax
    sub edx,4
    mov [edx],eax
    jmp ebx

wlast:
    db 6,"(last)"
    dd wrpl
    dd wlast_0
    dd compile
wlast_0:
    mov eax,[last]
    ;xor ebx,ebx
    ;mov bl,[eax]
    ;inc eax
    ;add eax,ebx
    sub edx,4
    mov [edx],eax
    ret

wbase:
    db 4,"base"
    dd wlast
    dd wbase_0
    dd compile
wbase_0:
    sub edx,4
    mov dword [edx],base
    ret

wherep:
    db 6,"(here)"
    dd wbase
    dd wherep_0
    dd compile
wherep_0:
    sub edx,4
    mov dword [edx],here
    ret

wsetadd:
    db 2,"+!"
    dd wherep
    dd wsetadd_0
    dd compile
wsetadd_0:
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    add edx,4
    add [ebx],eax
    ret

wset:
    db 1,"!"
    dd wsetadd
    dd wset_0
    dd compile
wset_0:
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    add edx,4
    mov [ebx],eax
    ret

wget:
    db 1,"@"
    dd wset
    dd wget_0
    dd compile
wget_0:
    mov eax,[edx]
    mov eax,[eax]
    mov [edx],eax
    ret

wsetc:
    db 2,"c!"
    dd wget
    dd wsetc_0
    dd compile
wsetc_0:
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    add edx,4
    mov [ebx],al
    ret

wgetc:
    db 2,"c@"
    dd wsetc
    dd wgetc_0
    dd compile
wgetc_0:
    mov ebx,[edx]
    xor eax,eax
    mov al,[ebx]
    mov [edx],eax
    ret

wsave:
    db 4,"save"
    dd wgetc
    dd wsave_0
    dd compile
wsave_0:
    push edx
    call getnext
    pop edx
    jmp save

wrestore:
    db 7,"restore"
    dd wsave
    dd wrestore_0
    dd compile
wrestore_0:
    push edx
    call getnext
    pop edx
    jmp restore

winclude:
    db 7,"include"
    dd wrestore
    dd winclude_0
    dd compile
winclude_0:
    push edx
    call getnext
    pop edx
    jmp include

wstatus:
    db 6,"status"
    dd winclude
    dd wstatus_0
    dd compile
wstatus_0:
    sub edx,4
    mov dword [edx],status
    ret

wputn:
    db 1,"."
    dd wstatus
    dd wputn_0
    dd compile
wputn_0:
    mov eax,[edx]
    add edx,4
    jmp puth

wliteral:
    db 7,"literal"
    dd wputn
    dd wliteral_0
    dd wliteral_0
wliteral_0:
    mov eax,[edx]
    add edx,4
    jmp literal

wand:
    db 3,"and"
    dd wliteral
    dd wand_0
    dd compile
wand_0:
    mov eax,[edx]
    add edx,4
    and [edx],eax
    ret

wor:
    db 2,"or"
    dd wand
    dd wor_0
    dd compile
wor_0:
    mov eax,[edx]
    add edx,4
    or [edx],eax
    ret

wxor:
    db 3,"xor"
    dd wor
    dd wxor_0
    dd compile
wxor_0:
    mov eax,[edx]
    add edx,4
    xor [edx],eax
    ret

wcompile:
    db 8,"compile,"
    dd wxor
    dd wcompile_0
    dd compile
wcompile_0:
    mov ecx,[edx]
    add edx,4
    jmp compile

wexecute:
    db 7,"execute"
    dd wcompile
    dd wexecute_0
    dd compile
wexecute_0:
    mov eax,[edx]
    add edx,4
    add eax,4
    jmp [eax]

wpostpone:
    db 8,"postpone"
    dd wexecute
    dd wpostpone_0
    dd wpostpone_0
wpostpone_0:
    push edx
    call getnext
    call findword
    pop edx
    jz notfound
    mov edi,[here]
    mov eax,ecx
    add eax,8
    mov eax,[eax]
    cmp eax,compile
    jz wpostpone_1

    add ecx,4
    jmp compile

wpostpone_1:
    mov eax,ecx
    ;add eax,4
    ;mov eax,[eax]
    call literal
    mov ecx,wcompile_0-12
    jmp compile

wtick:
    db 1,"'"
    dd wpostpone
    dd wtick_0
    dd compile
wtick_0:
    push edx
    call getnext
    call findword
    pop edx
    jz notfound
    sub edx,4
    mov [edx],ecx
    ret

wdivmod:
    db 4,"/mod"
    dd wtick
    dd wdivmod_0
    dd compile
wdivmod_0:
    mov ecx,edx
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    xor edx,edx
    div ebx
    mov ebx,edx
    mov edx,ecx
    mov [edx],eax
    add ecx,4
    mov [ecx],ebx
    ret

wmul:
    db 1,"*"
    dd wdivmod
    dd wmul_0
    dd compile
wmul_0:
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    mov ecx,edx
    mul ebx
    mov edx,ecx
    mov [edx],eax
    ret

wparsec:
    db 6,"parsec"
    dd wmul
    dd wparsec_0
    dd compile
wparsec_0:
    push edx
    call [nextfun]
    pop edx
    sub edx,4
    mov [edx],eax
    ret

wsbuf:
    db 4,"sbuf"
    dd wparsec
    dd wsbuf_0
    dd compile
wsbuf_0:
    sub edx,4
    mov dword [edx],sbuf
    ret

wnbuf:
    db 4,"nbuf"
    dd wsbuf
    dd wnbuf_0
    dd compile
wnbuf_0:
    sub edx,4
    mov dword [edx],nbuf
    ret

lastinit: equ wnbuf

    section .bss

nextfun: resd 1
here: resd 1
stack: resd 512
stack_end:
mstack: resd 64
mstack_end:
msp: resd 1
fd: resd 1
last: resd 1
newlast: resd 1
status: resd 1
base: resd 1
nextbuf: resb 512
buf1: resb 80
temp1: resd 1
temp2: resd 1
temp3: resd 1
sbuf: resb 1024*72
nbuf: resb 80
dict: resb 1024*512
