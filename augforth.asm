; forth interpreter for x86 linux

    section .text
    global _start

sys_exit: equ 1
sys_write: equ 4
sys_read: equ 3
sys_write: equ 4
sys_open: equ 5
sys_close: equ 6
sys_ftruncate: equ 93

file_writeflags: equ 0102o
file_readflags: equ 0000o
file_mode: equ 0600o

_start:
    mov edx,stack_end
    mov dword [last],lastinit
    mov dword [here],dict
    mov dword [status],0
    mov dword [base],10
    mov dword [nextfun],getnextc
    mov dword [ssp],sstack_end

    mov ebx,core_filename
    call include_0

    pop ecx
    pop eax
l0:
    dec ecx
    jz interpreter
    pop ebx
    push ecx
    call include_0
    pop ecx
    jmp l0

core_filename:
    db "core.f",0

include:
    call nullterm

include_0:
    mov esi,edx

    mov eax,sys_open
    mov ecx,file_readflags
    mov edx,file_mode
    int 0x80
    cmp eax,-2
    jz failopen

    mov ebx,[nextfun]
    push ebx
    mov ebx,[fd]
    push ebx
    push esi
    mov [fd],eax
    mov dword [nextfun],fgetnextc

    sub dword [ssp],4
    mov [ssp],esp
    mov edx,esi
    jmp interpreter

include_1:
    mov esp,[ssp]
    add dword [ssp],4

    mov ebx,[fd]
    mov eax,sys_close
    int 0x80

    pop edx
    pop eax
    mov [fd],eax
    pop eax
    mov [nextfun],eax

include_2:
    ret

fgetnextc:
    mov eax,sys_read
    mov ebx,[fd]
    mov ecx,esp
    dec ecx
    mov edx,1
    int 0x80
    xor eax,eax
    mov al,[ecx]

    push eax ; < ~~~  magic
    pop eax  ; <

    or al,al
    jz include_1

    ret

fgetnextc_0:
    or esi,esi
    jz include_1

    ret

restore:
    push edx

    call nullterm
    mov eax,sys_open
    mov ecx,file_readflags
    mov edx,file_mode
    int 0x80
    cmp eax,-2
    jz failopen

    mov ebx,eax

    mov eax,sys_read
    mov ecx,esp
    sub ecx,4
    mov edx,4
    int 0x80
    mov eax,[ecx]
    add eax,dict
    mov [here],eax

    mov eax,sys_read
    mov edx,4
    int 0x80
    mov eax,[ecx]
    add eax,dict
    mov [last],eax

    mov eax,sys_read
    mov ecx,dict
    mov edx,0xffffffff
    int 0x80

    mov eax,sys_close
    int 0x80

    pop edx
    ret

save:
    push edx

    call nullterm
    mov eax,sys_open
    mov ecx,file_writeflags
    mov edx,file_mode
    int 0x80
    cmp eax,-2
    jz failopen

    mov ebx,eax

    mov eax,sys_ftruncate
    xor ecx,ecx
    int 0x80

    mov ecx,esp
    sub ecx,4
    mov eax,[here]
    sub eax,dict
    mov [ecx],eax
    mov eax,sys_write
    mov edx,4
    int 0x80

    mov eax,[last]
    sub eax,dict
    mov [ecx],eax
    mov eax,sys_write
    mov edx,4
    int 0x80

    mov eax,sys_write
    mov ecx,dict
    mov edx,[here]
    sub edx,ecx
    int 0x80

    mov eax,sys_close
    int 0x80

    pop edx
    ret

failopen:
    mov eax,failopen_msg
    mov ebx,failopen_msg_end-failopen_msg
    call putsn
    mov eax,nextbuf
    call count
    call putsn
    mov al,10
    call putc

    mov eax,sys_exit
    int 0x80

failopen_msg:
    db "failed to open "
failopen_msg_end:

nullterm:
    mov edi,nextbuf
    mov esi,eax
    mov ecx,ebx
    mov ebx,edi
    rep movsb
    xor al,al
    stosb
    ret

interpreter:
    call getnext
    call findword
    jz interpreter_n

    push interpreter
    jmp ecx

interpreter_n:
    call number
    jz notfound
    mov eax,ecx
    push interpreter
    jmp literal

notfound:
    call putsn
    mov eax,notfound_msg
    mov ebx,notfound_msg_end-notfound_msg
    call putsn

cancelcol:
    mov eax,[status]
    or eax,eax
    jz cancelcol_0

    mov eax,[newlast]
    mov [here],eax

cancelcol_0:
    ret

notfound_msg:
    db " ?",10,0
notfound_msg_end:

literal:
    mov ebx,[status]
    or ebx,ebx
    jnz literal_0

    sub edx,4
    mov [edx],eax
    ret

literal_0:
    mov edi,[here]

    ; sub edx,4 ; mov dword [edx],#
    mov dword [edi],0xc704ea83
    add edi,4
    mov byte [edi],0x02
    inc edi

    stosd
    mov [here],edi
    ret

number:
    call storeabd

    or ebx,ebx
    jz number_n

    mov esi,eax
    mov edi,ebx
    xor bl,bl
    xor ecx,ecx
    xor eax,eax

    mov al,[esi]
    cmp al,'-'
    jnz number_nn

number_neg:
    inc esi
    dec edi
    jz number_n

    inc bl
    jmp number_0

number_nn:
    cmp al,'#'
    jnz number_nd

    inc esi
    dec edi
    jz number_n
    mov eax,[base]
    push eax
    mov dword [base],10
    call number_nag
    pop esi
    mov [base],esi
    ret

number_nd:
    cmp al,'$'
    jnz number_nh

    inc esi
    dec edi
    jz number_n
    mov eax,[base]
    push eax
    mov dword [base],16
    call number_nag
    pop esi
    mov [base],esi
    ret

number_nh:
    cmp al,'%'
    jnz number_0

    inc esi
    dec edi
    jz number_n
    mov eax,[base]
    push eax
    mov dword [base],2
    call number_nag
    pop esi
    mov [base],esi
    ret

number_nag:
    mov al,[esi]
    cmp al,'-'
    jz number_neg

number_0:
    mov dl,[base]
    cmp dl,11
    jb number_b

    mov bh,[base]
    add bh,'a'

number_a0:
    lodsb
    cmp al,'0'
    jb number_a2
    cmp al,'9'
    ja number_a2

    sub al,'0'

number_a1:
    xchg ecx,eax
    mov edx,[base]
    mul edx
    xchg ecx,eax
    add ecx,eax
    dec edi
    jnz number_a0

    jmp number_t

number_a2:
    cmp al,'a'
    jb number_n
    cmp al,bh
    ja number_n

    sub al,'a'-10
    jmp number_a1

number_b:
    mov bh,[base]
    add bh,'0'

number_b0:
    lodsb
    cmp al,'0'
    jb number_n
    cmp al,bh
    ja number_n

    sub al,'0'
    xchg ecx,eax
    mov edx,[base]
    mul edx
    xchg ecx,eax
    add ecx,eax
    dec edi
    jnz number_b0

number_t:
    or bl,bl
    jz number_t0

    neg ecx

number_t0:
    call loadabd
    or esi,esi
    ret

number_n:
    call loadabd
    xor ecx,ecx
    ret

storeabd:
    mov [temp1],eax
    mov [temp2],ebx
    mov [temp3],edx
    ret

loadabd:
    mov eax,[temp1]
    mov ebx,[temp2]
    mov edx,[temp3]
    ret

getnext:
    push edx

getnext_0:
    mov edi,nextbuf
    mov esi,0

getnext_1:
    call [nextfun]
    cmp al,33
    jb getnext_2

    stosb
    inc esi
    jmp getnext_1

getnext_2:
    or esi,esi
    jnz getnext_3

    or al,al
    jnz getnext_0

getnext_3:
    pop edx

    mov ebx,esi
    mov eax,nextbuf
    or ebx,ebx
    jz getnext
    ret

getnextc:
    mov ecx,esp
    ;sub ecx,4
    dec ecx
    mov edx,1
    mov eax,sys_read
    mov ebx,2
    int 0x80
    mov al,[ecx]
    ret

findword:
    push edx
    push eax
    mov ecx,[last]
    xor edx,edx

findword_0:
    mov dl,[ecx]
    inc ecx
    call streq
    jz findword_1

    add ecx,edx
    mov ecx,[ecx]
    or ecx,ecx
    jnz findword_0

    pop eax
    pop edx
    ret ; fail

findword_1: ; found
    add ecx,edx
    add ecx,4
    or ecx,ecx
    pop eax
    pop edx
    ret

streq:
    pushad
    mov esi,eax
    cmp ebx,edx
    jnz streq_n

streq_0:
    lodsb
    mov ah,[ecx]
    cmp al,ah
    jnz streq_n

    inc ecx
    dec edx
    jnz streq_0

streq_t:
    xor edx,edx
    popad
    ret

streq_n:
    popad
    or eax,eax
    ret

count:
    mov esi,eax
    mov ebx,eax

count_0:
    lodsb
    or al,al
    jnz count_0

    sub esi,ebx
    mov eax,ebx
    mov ebx,esi
    ret

puth:
    push eax
    mov ebx,eax
    mov edi,buf1+8
    std

puth_0:
    mov eax,ebx
    shr ebx,4
    and al,0xf
    cmp al,10
    jb puth_1

    add al,'a'-10
    jmp puth_2

puth_1:
    add al,'0'
puth_2:
    stosb
    or ebx,ebx
    jnz puth_0

    cld
    inc edi
    mov eax,edi
    neg edi
    inc edi
    add edi,buf1+8
    mov ebx,edi
    call putsn

    pop eax
    ret

putc:
    pushad
    mov ecx,esp
    dec ecx
    mov [ecx],al
    mov edx,1
    mov eax,sys_write
    mov ebx,1
    int 0x80
    popad
    ret

putsn:
    pushad
    mov ecx,eax
    mov edx,ebx
    mov eax,sys_write
    mov ebx,1
    int 0x80
    popad
    ret

compile:
    mov eax,[status]
    or eax,eax
    jnz compile_0

    ret

compile_0:
    mov edi,[here]
    mov byte [edi],0xe8 ; call rel
    inc edi
    pop eax
    sub eax,edi
    sub eax,4
    stosd
    mov [here],edi
    ret

; dictionary

wemit:
    db 4,"emit"
    dd 0
    call compile
    mov ecx,esp
    sub ecx,4
    mov eax,[edx]
    add edx,4
    mov [ecx],eax
    mov eax,sys_write
    mov ebx,1
    mov edi,edx
    mov edx,4
    int 0x80
    mov edx,edi
    ret

wadd:
    db 1,"+"
    dd wemit
    call compile
    mov eax,[edx]
    add edx,4
    add [edx],eax
    ret

wcol:
    db 1,":"
    dd wadd
    call compile
    push edx
    call getnext
    pop edx
    mov edi,[here]
    mov [newlast],edi
    mov ecx,ebx
    mov esi,eax
    mov al,cl
    stosb
    rep movsb

    mov eax,[last]
    stosd
    mov al,0xe8 ; call
    stosb
    mov eax,compile
    sub eax,edi
    sub eax,4
    stosd

    mov [here],edi
    mov dword [status],1
    ret

wsemi:
    db 1,";"
    dd wcol
    jmp wsemi_0
wsemi_0:
    mov eax,[newlast]
    mov [last],eax
    mov dword [status],0
    mov edi,[here]
    mov al,0xc3
    stosb
    mov [here],edi
    ret

wswap:
    db 4,"swap"
    dd wsemi
    call compile
    mov ecx,edx
    add ecx,4
    mov eax,[edx]
    mov ebx,[ecx]
    mov [ecx],eax
    mov [edx],ebx
    ret

wdup:
    db 3,"dup"
    dd wswap
    call compile
    mov eax,[edx]
    sub edx,4
    mov [edx],eax
    ret

wover:
    db 4,"over"
    dd wdup
    call compile
    add edx,4
    mov eax,[edx]
    sub edx,8
    mov [edx],eax
    ret

wdrop:
    db 4,"drop"
    dd wover
    call compile
    add edx,4
    ret

wrph:
    db 2,">r"
    dd wdrop
    call compile
    mov eax,[edx]
    add edx,4
    pop ebx
    push eax
    jmp ebx

wrpl:
    db 2,"r>"
    dd wrph
    call compile
    pop ebx
    pop eax
    sub edx,4
    mov [edx],eax
    jmp ebx

wlast:
    db 6,"(last)"
    dd wrpl
    call compile
    mov eax,[last]
    sub edx,4
    mov [edx],eax
    ret

wbase:
    db 4,"base"
    dd wlast
    call compile
    sub edx,4
    mov dword [edx],base
    ret

wherep:
    db 6,"(here)"
    dd wbase
    call compile
    sub edx,4
    mov dword [edx],here
    ret

wsetadd:
    db 2,"+!"
    dd wherep
    call compile
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    add edx,4
    add [ebx],eax
    ret

wset:
    db 1,"!"
    dd wsetadd
    call compile
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    add edx,4
    mov [ebx],eax
    ret

wget:
    db 1,"@"
    dd wset
    call compile
    mov eax,[edx]
    mov eax,[eax]
    mov [edx],eax
    ret

wsetc:
    db 2,"c!"
    dd wget
    call compile
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    add edx,4
    mov [ebx],al
    ret

wgetc:
    db 2,"c@"
    dd wsetc
    call compile
    mov ebx,[edx]
    xor eax,eax
    mov al,[ebx]
    mov [edx],eax
    ret

wsave:
    db 4,"save"
    dd wgetc
    call compile
    push edx
    call getnext
    pop edx
    jmp save

wrestore:
    db 7,"restore"
    dd wsave
    call compile
    push edx
    call getnext
    pop edx
    jmp restore

winclude:
    db 7,"include"
    dd wrestore
    call compile
    push edx
    call getnext
    pop edx
    jmp include

wstatus:
    db 6,"status"
    dd winclude
    call compile
    sub edx,4
    mov dword [edx],status
    ret

wputn:
    db 1,"."
    dd wstatus
    call compile
    mov eax,[edx]
    add edx,4
    jmp puth

wliteral:
    db 7,"literal"
    dd wputn
    jmp wliteral_0
wliteral_0:
    mov eax,[edx]
    add edx,4
    jmp literal

wand:
    db 3,"and"
    dd wliteral
    call compile
    mov eax,[edx]
    add edx,4
    and [edx],eax
    ret

wor:
    db 2,"or"
    dd wand
    call compile
    mov eax,[edx]
    add edx,4
    or [edx],eax
    ret

wxor:
    db 3,"xor"
    dd wor
    call compile
    mov eax,[edx]
    add edx,4
    xor [edx],eax
    ret

wcompile:
    db 8,"compile,"
    dd wxor
    call compile
    mov ecx,[edx]
    add edx,4
    jmp compile

wexecute:
    db 7,"execute"
    dd wcompile
    call compile
    mov eax,[edx]
    add edx,4
    add edx,5
    jmp eax

wpostpone:
    db 8,"postpone"
    dd wexecute
    jmp wpostpone_0
wpostpone_0:
    call getnext
    call findword
    jz notfound
    push ecx
    jmp compile

wtick:
    db 1,"'"
    dd wpostpone
    call compile
    call getnext
    call findword
    jz notfound
    sub edx,4
    mov [edx],ecx
    ret

wdivmod:
    db 4,"/mod"
    dd wtick
    call compile
    mov ecx,edx
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    xor edx,edx
    div ebx
    mov ebx,edx
    mov edx,ecx
    mov [edx],eax
    add ecx,4
    mov [ecx],ebx
    ret

wmul:
    db 1,"*"
    dd wdivmod
    call compile
    mov ebx,[edx]
    add edx,4
    mov eax,[edx]
    mov ecx,edx
    mul ebx
    mov edx,ecx
    mov [edx],eax
    ret

wparsec:
    db 6,"parsec"
    dd wmul
    call compile
    push edx
    call [nextfun]
    pop edx
    sub edx,4
    mov [edx],eax
    ret

wsbuf:
    db 4,"sbuf"
    dd wparsec
    call compile
    sub edx,4
    mov dword [edx],sbuf
    ret

wbye:
    db 3,"bye"
    dd wsbuf
    call compile
    mov eax,sys_exit
    int 0x80

lastinit: equ wbye

    section .bss

nextfun: resd 1
here: resd 1
stack: resd 512
stack_end:
fd: resd 1
sstack: resd 32
sstack_end:
ssp: resd 1
last: resd 1
newlast: resd 1
status: resd 1
base: resd 1
nextbuf: resb 512
buf1: resb 80
temp1: resd 1
temp2: resd 1
temp3: resd 1
sbuf: resb 1024*72
dict: resb 1024*512
