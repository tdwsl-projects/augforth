
: 2dup over over ;
: rot >r swap r> swap ;
: -rot swap >r swap r> ;
: 2drop drop drop ;
: nip >r drop r> ;
: r@ r> r> dup >r swap >r ;

: 1+ 1 + ;
: 1- -1 + ;
: invert $ffffffff xor ;
: negate invert 1+ ;
: - negate + ;

: cr 10 emit ;
: space 32 emit ;

: here (here) @ ;
: allot (here) +! ;

: , here ! 4 allot ;
: c, here c! 1 allot ;

: last (last) dup c@ + 1+ 4 + ;
: immediate $e9 last c! 0 last 1+ ! ;

: [ 0 status ! ; immediate
: ] 1 status ! ;
: ]l ] postpone literal ;
: compile? status @ ;

: hex 16 base ! ;
: decimal 10 base ! ;

: constant : postpone literal postpone ; ;
: create : 0 postpone literal here 4 - postpone ; here swap ! ;
: variable create 4 allot ;

: jz, $c283028b , $0fc00904 , $84 c, here 4 + - , ;
: j, $e9 c, here 4 + - , ;
: j! dup >r 4 + - r> ! ;

: if r> 0 jz, here 4 - >r >r ; immediate
: then r> here r> j! >r ; immediate
: else r> 0 j, here r> j! here 4 - >r >r ; immediate

: begin r> here >r >r ; immediate
: until r> r> jz, >r ; immediate
: again r> r> j, >r ; immediate
: while 0 jz, r> here 4 - >r >r ; immediate
: repeat r> r> r> j, here swap j! >r ; immediate

: value constant ;
: to ' 10 + compile? if postpone literal postpone ! else ! ; immediate

: ?dup dup if dup then ;

: 0= if 0 else -1 then ;
: 0<> if -1 else 0 then ;
: 0< $80000000 and 0<> ;
: 0>= $80000000 and 0= ;
: 0<= 1- $80000000 and 0<> ;
: 0> 1- $80000000 and 0= ;
: < - 0< ;
: >= - 0>= ;
: <= - 0<= ;
: > - 0> ;
: = - 0= ;
: <> - 0<> ;

: min 2dup < if drop else nip then ;
: max 2dup > if drop else nip then ;

: / /mod nip ;
: mod /mod drop ;

variable sbufp sbuf sbufp !
variable nsbufp

: white? 32 <= ;

: c>buf >r r@ @ c! 1 r> +! ;

: parse-next
  begin parsec dup white? while drop repeat
  sbufp @ nsbufp !
  nsbufp c>buf
  begin parsec dup white? 0= while nsbufp c>buf repeat drop
  sbufp @ nsbufp @ over - ;

: char parse-next drop c@ ;
: [char] char postpone literal ; immediate

: ( begin parsec [char] ) = until ; immediate
: \ begin parsec dup 10 = swap 0= or until ; immediate

: s1+ 1- swap 1+ swap ;
: type begin dup while over c@ emit s1+ repeat 2drop ;

: preserve sbufp +! ;

: parse-until
  sbufp @ nsbufp !
  >r begin parsec dup r@ <> while nsbufp c>buf repeat r> 2drop
  sbufp @ nsbufp @ over - ;

: s" [char] " parse-until dup preserve
  compile? if swap postpone literal postpone literal then ; immediate

( doesn't work :(
: ." [char] " parse-until
 compile? if dup preserve swap postpone literal postpone literal postpone type
 else type then ; immediate
)
: ." postpone s" postpone type ; immediate

: words
  0 >r
  (last) begin
    dup c@ over 1+ swap
    dup r> + 1+ dup 80 >= if drop cr dup then >r
    type space
    dup c@ + 1+ @
  dup 0= until
  r> 2drop cr ;

variable nbufp

: .digit ( u -- )
  dup 10 < if [char] 0 else [ char a 10 - ]l then + emit ;

: .nbuf
  nbufp @
  begin 1- dup c@ .digit dup sbuf = until
  drop ;

: #nbuf ( -- u ) nbufp @ sbuf - ;

: spaces 0 max begin dup while 1- space repeat drop ;

: >nbuf ( u -- )
  sbuf nbufp !
  begin
    base @ /mod swap
    nbufp c>buf
  ?dup 0= until ;

: -? ( n -- u tf ) dup 0< dup if >r negate r> then ;

: .r swap -? >r >nbuf
  #nbuf r@ - - spaces
  r> if [char] - emit then .nbuf ;

: . -? if [char] - emit then >nbuf .nbuf space ;

." ready" cr
